cmake_minimum_required(VERSION 3.5)
project(pipeline)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES pipeline.c)
add_executable(so ${SOURCE_FILES})